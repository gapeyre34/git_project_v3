<div class="container">
    <div class="container justify-center align-items-center">
        <div class="d-flex justify-center align-items-center">
            <form action="{{ route('recettes.store') }}" method="post" class="w-50">
                @csrf
                <fieldset>
                    <h3 class="text-center">Ajout d'une recette</h3>
                    <div class="form-group">
                        <label class="form-label mt-4">Nom de la recette</label>
                        <input id="recette_name" name="recette_name" type="text" class="form-control">
                    </div>
                    <div class="form-group">
                        <label class="form-label mt-4">URL de l'image de la recette</label>
                        <input id="path_img" name="path_img" type="text" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1" class="form-label mt-4">Temps de préparation</label>
                        <input id="prep_timing" name="prep_timing" type="text" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1" class="form-label mt-4">Commentaire</label>
                        <textarea id="prep_com" name="prep_com" class="form-control" rows="3"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="exampleSelect1" class="form-label mt-4">Difficulté</label>
                        <select id="difficulty_id" name="difficulty_id" class="form-select">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                        </select>
                    </div>
                    <div class="container border rounded bg-light py-3 my-4">
                        <h5>Ajouter des ingrédients</h5>
                        <div class="form-group">
                            <label for="exampleSelect1" class="form-label mt-4">Nombre d'ingrédients</label>
                            <select id="ingredient_nbr" wire:model="ingredientSelected" name="ingredient_nbr" class="form-select">
                                @foreach($ingredients as $key=>$ingredient)
                                    <option value="{{ $key }}" > {{ $key+1}} </option>
                                @endforeach
                            </select>
                        </div>
                        @for($i=0; $i <= $ingredientSelected; $i++)
                            <div class="form-group my-3">
                                <label for="exampleSelect1" class="form-label {{$ingredientSelected}}">Ingrédient n°{{$i+1}}</label>
                                <select class="form-select" name="ingredient_id{{$i}}">
                                    @foreach($ingredients as $ingredient)
                                        <option value="{{ $ingredient->id }}">{{ $ingredient->ingredient_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        @endfor
                    </div>
                    <div class="container border rounded bg-light pt-3 my-4">
                        <h5>Ajouter des étapes</h5>
                        <div class="form-group">
                            <label for="exampleSelect1" class="form-label mt-4">Nombre d'étapes</label>
                            <select id="etape_nbr" name="etape_nbr" wire:model="etapeSelected" class="form-select">
                                <option value= 1 >1</option>
                                <option value= 2 >2</option>
                                <option value= 3 >3</option>
                                <option value= 4 >4</option>
                                <option value= 5 >5</option>
                            </select>
                        </div>
                        @for($i=1; $i <= $etapeSelected; $i++)
                            <div class="container border rounded pb-3 my-3 bg-white">
                                <div class="form-group">
                                    <label class="form-label mt-4">Nom de l'étape</label>
                                    <input id="etape_libelle" name="etape_libelle{{$i}}" type="text" class="form-control" placeholder="Etape n°{{$i}}/{{$etapeSelected}}">
                                </div>
                                <div class="form-group">
                                    <label class="form-label mt-4">Numéro de l'étape (ordonnancement)</label>
                                    <input id="etape_order" name="etape_order{{$i}}" type="number" class="form-control" placeholder="{{$i}}/{{$etapeSelected}}">
                                </div>
                                <div class="form-group">
                                    <label class="form-label mt-4">Description</label>
                                    <textarea id="etape_desc" name="etape_desc{{$i}}" type="text" class="form-control" placeholder="Etape n°{{$i}}/{{$etapeSelected}}: Cuisinez!"></textarea>
                                </div>
                            </div>
                        @endfor
                    </div>
                    <div class="d-flex justify-content-center">
                        <button class="bg-secondary mb-5 text-white font-bold py-2 px-4 rounded">Accept</button>
                    </div>

                </fieldset>

            </form>
        </div>
    </div>
    @livewireScripts
</div>
