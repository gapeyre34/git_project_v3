<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ingredients extends Model
{
    use HasFactory;

    protected $fillable = [
        'ingredient_name',
        'allergen',
    ];

    public function recettes()
    {
        return $this->belongsToMany(Recettes::class);
    }
}
