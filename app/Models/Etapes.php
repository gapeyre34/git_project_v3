<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Etapes extends Model
{
    use HasFactory;

    protected $fillable = [
        'etape_libelle',
        'etape_order',
        'etape_desc',
        'recettes_id',
    ];

    public function recette()
    {
        return $this->belongsTo(Recettes::class);
    }
}
