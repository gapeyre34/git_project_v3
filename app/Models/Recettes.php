<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Recettes extends Model
{
    use HasFactory;

    protected $fillable = [
        'recette_name',
        'prep_timing',
        'prep_com',
        'path_img',
        'difficulty_id',
    ];

    public function etapes()
    {
        return $this->hasMany(Etapes::class);
    }

    public function ingredients()
    {
        return $this->belongsToMany(Ingredients::class);
    }

    public function difficulty()
    {
        return $this->belongsTo(Difficulty::class);
    }
}
