<?php

namespace Database\Seeders;

use App\Models\Recettes;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class IngredientsRecettesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Recettes::find(1)->ingredients()->attach(5);
        Recettes::find(1)->ingredients()->attach(6);
        Recettes::find(1)->ingredients()->attach(13);
        Recettes::find(1)->ingredients()->attach(7);
        Recettes::find(1)->ingredients()->attach(8);
        Recettes::find(1)->ingredients()->attach(9);
        Recettes::find(1)->ingredients()->attach(10);
        Recettes::find(1)->ingredients()->attach(11);
        Recettes::find(1)->ingredients()->attach(12);
    }
}
