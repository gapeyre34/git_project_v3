<?php

namespace Database\Seeders;

use App\Models\Difficulty;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DifficultySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $difficulty = new Difficulty();
        $difficulty->id = 1;
        $difficulty->value = 1;
        $difficulty->description = "Pour les néophytes de la cuisine";
        $difficulty->save();

        $difficulty = new Difficulty();
        $difficulty->id = 2;
        $difficulty->value = 2;
        $difficulty->description = "Pour les amateurs de cuisine";
        $difficulty->save();

        $difficulty = new Difficulty();
        $difficulty->id = 3;
        $difficulty->value = 3;
        $difficulty->description= "Pour les expérimentés de la cuisine";
        $difficulty->save();

        $difficulty = new Difficulty();
        $difficulty->id = 4;
        $difficulty->value = 4;
        $difficulty->description = "Pour les maîtres de la cuisine";
        $difficulty->save();

        $difficulty = new Difficulty();
        $difficulty->id = 5;
        $difficulty->value = 5;
        $difficulty->description = "Pour les grands chefs";
        $difficulty->save();
    }
}
