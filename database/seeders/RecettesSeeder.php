<?php

namespace Database\Seeders;

use App\Models\Recettes;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class RecettesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $recette = new Recettes();
        $recette->recette_name = 'Gardianne de Taureau';
        $recette->prep_timing = 210;
        $recette->prep_com = "A préparer 3 jours à l'avance";
        $recette->difficulty_id = 3;
        $recette->save();
    }
}
