<?php

namespace Database\Seeders;

use App\Models\Ingredients;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class IngredientsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $ingredient = new Ingredients();
        $ingredient->id = 1;
        $ingredient->ingredient_name = "Miel";
        $ingredient->allergen = false;
        $ingredient->save();

        $ingredient = new Ingredients();
        $ingredient->id = 2;
        $ingredient->ingredient_name = "Vinaigre balsamique";
        $ingredient->allergen = false;
        $ingredient->save();

        $ingredient = new Ingredients();
        $ingredient->id = 3;
        $ingredient->ingredient_name = "Sel";
        $ingredient->allergen = false;
        $ingredient->save();

        $ingredient = new Ingredients();
        $ingredient->id = 4;
        $ingredient->ingredient_name = "Magret de Canard";
        $ingredient->allergen = false;
        $ingredient->save();

        $ingredient = new Ingredients();
        $ingredient->id = 5;
        $ingredient->ingredient_name = "Huilde d'olive";
        $ingredient->allergen = false;
        $ingredient->save();

        $ingredient = new Ingredients();
        $ingredient->id = 6;
        $ingredient->ingredient_name = "Vin rouge deCahors";
        $ingredient->allergen = false;
        $ingredient->save();

        $ingredient = new Ingredients();
        $ingredient->id = 7;
        $ingredient->ingredient_name = "Gousse d'ail";
        $ingredient->allergen = false;
        $ingredient->save();

        $ingredient = new Ingredients();
        $ingredient->id = 8;
        $ingredient->ingredient_name = "Thym";
        $ingredient->allergen = false;
        $ingredient->save();

        $ingredient = new Ingredients();
        $ingredient->id = 9;
        $ingredient->ingredient_name = "Poivre";
        $ingredient->allergen = false;
        $ingredient->save();

        $ingredient = new Ingredients();
        $ingredient->id = 10;
        $ingredient->ingredient_name = "Olives 'picholines'";
        $ingredient->allergen = false;
        $ingredient->save();

        $ingredient = new Ingredients();
        $ingredient->id = 11;
        $ingredient->ingredient_name = "Laurier";
        $ingredient->allergen = false;
        $ingredient->save();

        $ingredient = new Ingredients();
        $ingredient->id = 12;
        $ingredient->ingredient_name = "Lard en dés";
        $ingredient->allergen = false;
        $ingredient->save();

        $ingredient = new Ingredients();
        $ingredient->id = 13;
        $ingredient->ingredient_name = "Oignon";
        $ingredient->allergen = false;
        $ingredient->save();
    }
}
