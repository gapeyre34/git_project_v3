<?php

namespace Database\Seeders;

use App\Models\Difficulty;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call([
            IngredientsSeeder::class,
            RecettesSeeder::class,
            IngredientsRecettesSeeder::class,
            EtapesSeeder::class,
            DifficultySeeder::class,
        ]);
    }
}
