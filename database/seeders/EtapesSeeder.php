<?php

namespace Database\Seeders;

use App\Models\Etapes;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class EtapesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $etape = new Etapes();
        $etape->id = 1;
        $etape->etape_libelle = "La marinade";
        $etape->etape_order = 1;
        $etape->etape_desc = "Faire une marinade avec tous les ingrédients, laisser mariner 2 jours en retournant régulièrement la viande.";
        $etape->recettes_id = 1;
        $etape->save();

        $etape = new Etapes();
        $etape->id = 2;
        $etape->etape_libelle = "Egoutage";
        $etape->etape_order = 2;
        $etape->etape_desc = "Egoutter le tout et séparer la viande des ingrédients de la marinade.";
        $etape->recettes_id = 1;
        $etape->save();

        $etape = new Etapes();
        $etape->id = 3;
        $etape->etape_libelle = "Faire revenir les ingrédients";
        $etape->etape_order = 3;
        $etape->etape_desc = "Faire revenir ces ingrédients dans l'huile d'olive, réserver.";
        $etape->recettes_id = 1;
        $etape->save();

        $etape = new Etapes();
        $etape->id = 4;
        $etape->etape_libelle = "Melangeons";
        $etape->etape_order = 4;
        $etape->etape_desc = "Verser 2 bonnes cuillères de farine dans l'huile chaude et liez en versant tout le vin de la marinade (ne pas ajouter d'eau).";
        $etape->recettes_id = 1;
        $etape->save();

        $etape = new Etapes();
        $etape->id = 5;
        $etape->etape_libelle = "La viande";
        $etape->etape_order = 5;
        $etape->etape_desc = "Remettre la viande et les ingrédients qui avaient été reservés dans cette sauce et cuire à feu très doux (niveau 2) environ trois heures.";
        $etape->recettes_id = 1;
        $etape->save();
    }
}
