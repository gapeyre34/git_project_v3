<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});
*/

Route::group([], function () {
   Route::get('/', function () {return view('dashboard.index');});
   Route::resource('recettes', \App\Http\Controllers\RecettesController::class);
   Route::resource('dashboard', \App\Http\Controllers\DashboardController::class);
   Route::resource('ingredients', \App\Http\Controllers\IngredientsController::class);
});
